#include <cvwidget.hh>

#include <QFileDialog>
#include <QStack>
#include <QDebug>

CVWidget::CVWidget(QObject *parent)
  : QThread(parent),
    outputType_m(VIDEO),
    startFlag_m(false),
    buffer_m(NULL),
    h_m(0),
    s_m(0),
    v_m(0),
    tolerance_m(10),
    threshold_m(100),
    show_match_m(true),
    rect_m(0, 0, 100, 100),
    use_roi_m(false),
    size_m(100, 100)
{
  timer_m.start();
  time_m = timer_m.elapsed();

  for (int i = 0; i < 8; ++i)
    neighbors_m.push_back(cv::Point(0, 0));
}

CVWidget::~CVWidget()
{
  close();
}

void CVWidget::labelize(cv::Mat& mask)
{
  QStack<cv::Point> stack;
  QList<cv::Point> biggest_cloud;
  QColor rc;
  cv::Vec3b vcolor;

  cloud_m.clear();

  for (int x = 0; x < mask.cols; x++)
  {
    for(int y = 0; y < mask.rows; y++)
    {
      if (mask.at<unsigned char>(y, x) == 255) // not marked
      {
        biggest_cloud.clear();
        stack.append(cv::Point(x, y));
        rc = random_color();
        vcolor = cv::Vec3b(rc.blue(), rc.green(), rc.red());

        while (!stack.isEmpty())
        {
          cv::Point p = stack.pop();
          // already marked
          if (mask.at<unsigned char>(p.y, p.x) == 0)
            continue;

          mask.at<unsigned char>(p.y, p.x) = 0;
          if (outputType_m == MASK)
          {
            if (use_roi_m)
              frame_m(rect_m).at<cv::Vec3b>(p.y, p.x) = vcolor;
            else
              frame_m.at<cv::Vec3b>(p.y, p.x) = vcolor;
          }

          biggest_cloud.append(cv::Point(p.x, p.y));

          // iterates on all neighbors of the pixel
          neighbors(p.x, p.y);
          foreach (cv::Point pn, neighbors_m)
          {
            if (pn.x >= 0 && pn.x < mask.cols
                && pn.y >= 0 && pn.y < mask.rows)
            {
              if (mask.at<unsigned char>(pn.y, pn.x) == 255)
              {
                stack.append(cv::Point(pn.x, pn.y));
              }
            }
          }
        }

        // found a bigger cloud?
        if (biggest_cloud.size() > cloud_m.size())
          cloud_m = biggest_cloud;
      }
    }
  }
}

cv::Point CVWidget::binarize()
{
  cv::Mat mask;
  cv::Mat hsv;

  cv::Mat frame_roi;
  if (use_roi_m)
    frame_roi = frame_m(rect_m).clone();
  else
    frame_roi = frame_m.clone();

  // create the HSV matrix
  hsv = frame_roi;
  cv::cvtColor(frame_roi, hsv, CV_BGR2HSV);

  mask.create(frame_roi.rows, frame_roi.cols, CV_8UC1); // b&w mask

  // check hsv range
  cv::inRange(hsv,
              cv::Scalar(h_m - tolerance_m - 1, s_m - tolerance_m, 0),
              cv::Scalar(h_m + tolerance_m -1, s_m + tolerance_m, 255),
              mask);

  // dilate and erode the mask
  cv::Mat kernel =  cv::getStructuringElement(cv::MORPH_ELLIPSE,
                                              cv::Size(5, 5),
                                              cv::Point(2, 2));
  kernel.setTo(cv::Scalar(1));
  cv::dilate(mask, mask, kernel);
  cv::erode(mask, mask, kernel);


  // if we're visualizing the mask, empty the frame with black
  if (outputType_m == MASK)
  {
    for(int x = 0; x < frame_m.cols; x++)
    {
      for(int y = 0; y < frame_m.rows; y++)
      {
        frame_m.at<cv::Vec3b>(y, x) = cv::Vec3b(0, 0, 0);
      }
    }
  }

  // labelize all regions
  cv::Mat label_mask = mask.clone();
  labelize(label_mask);
  label_mask.release();

  int sum_x = 0;
  int sum_y = 0;
  n_pixels_m = 0;
  cv::Vec3b vcolor;

  if (outputType_m == MASK)
    vcolor = cv::Vec3b(255, 255, 255);
  else
    vcolor = cv::Vec3b(0, 255, 0);
  // for each point of the cloud, sum coordinates
  foreach (cv::Point p, cloud_m)
  {
    if (show_match_m)
    {
      if (use_roi_m)
        frame_m(rect_m).at<cv::Vec3b>(p.y, p.x) = vcolor;
      else
        frame_m.at<cv::Vec3b>(p.y, p.x) = vcolor;
    }

    if (use_roi_m)
    {
      sum_x += p.x + rect_m.x;
      sum_y += p.y + rect_m.y;
    }
    else
    {
      sum_x += p.x;
      sum_y += p.y;
    }
    ++n_pixels_m;
  }

  kernel.release();
  hsv.release();
  mask.release();

  if (n_pixels_m > 0)
    return cv::Point(sum_x / n_pixels_m, sum_y / n_pixels_m);

  return pos_m;
}

void CVWidget::changeColor(int x, int y)
{
  cv::Vec3b   color;

  mutex_m.lock();
  {
    if (frame_m.cols > 0 && frame_m.rows > 0)
    {
      pos_m = cv::Point(x, y);
      color = frame_m.at<cv::Vec3b>(y, x);
      color_m = QColor(color[2], color[1], color[0]);
      emit colorChanged(color_m);

      cv::Mat hsv = frame_m.clone();

      cv::cvtColor(frame_m, hsv, CV_BGR2HSV);
      cv::Vec3b pixel = hsv.at<cv::Vec3b>(y, x);
      h_m = pixel[0];
      s_m = pixel[1];
      v_m = pixel[2];

      hsv.release();
    }
  }
  mutex_m.unlock();
}

void CVWidget::run()
{
    int current_time;
    int delta;
    GLWidget::s_cube_pos cube_pos;

    while (startFlag_m)
    {
        current_time = timer_m.elapsed();
        delta = current_time - time_m;
        cv::Point p;

        if (delta > 0)
            emit fpsUpdate(1000/delta);

        if (delta > 30)
        {
            mutex_m.lock();
            {
                in_m >> frame_m;

                if (rect_buffer_m->dataAvailable())
                {
                    rect_m = rect_buffer_m->get();
                    checkRect();
                    size_m.width = rect_m.width;
                    size_m.height = rect_m.height;
                }

                if (frame_m.cols > 0 && frame_m.rows > 0)
                {
                    p = binarize();

                    if (p.x >= 0 && p.y >= 0)
                    {
                        if (use_roi_m)
                            setRectPos(p.x-rect_m.width/2, p.y-rect_m.width/2);

                        if (use_roi_m && outputType_m != MASK)
                            cv::rectangle(frame_m, rect_m, CV_RGB(0, 0, 255));

                        cube_pos.x = (float)p.x / frame_m.cols;
                        cube_pos.y = (float)p.y / frame_m.rows;
                        gl_buffer_m->add(cube_pos);
                    }

                    buffer_m->add(frame_m.clone());

                    if (videoType_m == FILE)
                    {
                        int pos = in_m.get(CV_CAP_PROP_POS_FRAMES);
                        emit newPos(pos);
                    }
                }
                time_m = current_time;
            }
            mutex_m.unlock();
        }

        if (frame_m.cols == 0)
            stop();
    }

    stop();
}


void CVWidget::open()
{
  mutex_m.lock();
  {
    if (in_m.isOpened())
    {
      in_m >> frame_m;
      buffer_m->add(frame_m.clone());
    }
  }
  mutex_m.unlock();

  emit opened();
  emit totalFrame(in_m.get(CV_CAP_PROP_FRAME_COUNT));
}

void CVWidget::play()
{
  bool ok = false;
  mutex_m.lock();
  {
    if ((ok = !startFlag_m))
    {
      startFlag_m = true;
    }
  }
  mutex_m.unlock();

  if (ok)
  {
    start();
    emit played();
  }
}

void CVWidget::close()
{
  stop();

  if (in_m.isOpened())
    in_m.release();
  emit closed();
}

void CVWidget::stop()
{
  if (startFlag_m)
  {
    mutex_m.lock();
    {
      startFlag_m = false;
    }
    mutex_m.unlock();
    wait();
    buffer_m->clear();
    emit stopped();
  }
}

void CVWidget::openFile()
{
  mutex_m.lock();
  {
    in_m.open(file_m.toStdString());
    videoType_m = FILE;
  }
  mutex_m.unlock();
  open();
}

void CVWidget::openDevice()
{
  stop();
  mutex_m.lock();
  {
    in_m.open(0);
    videoType_m = DEVICE;
  }
  mutex_m.unlock();
  open();
}

void CVWidget::openVideo()
{
  stop();
  file_m = QFileDialog::getOpenFileName(NULL,
                                        tr("Open File"),
                                        "/home",
                                        tr("Videos (*.avi *.wmv *.mp4 *.flv)"));

  if (!file_m.isEmpty())
    openFile();
}
