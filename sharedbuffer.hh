#ifndef SHAREDBUFFER_HH
#define SHAREDBUFFER_HH

# include <QQueue>
# include <QMutex>

# include <opencv/cv.h>
# include <opencv/highgui.h>

template <typename T>
class SharedBuffer
{
public:
    SharedBuffer() {}
    ~SharedBuffer()
    {
        clear();
    }

    bool dataAvailable()
    {
        bool ret;
        mutex_m.lock();
        {
            ret = !queue_m.isEmpty();
        }
        mutex_m.unlock();

        return ret;
    }

    T get()
    {
        T object;
        if (dataAvailable())
        {
            mutex_m.lock();
            {
                object = queue_m.dequeue();
            }
            mutex_m.unlock();
        }

        return object;
    }

    void add(T const& object)
    {
        mutex_m.lock();
        {
            queue_m.enqueue(object);
        }
        mutex_m.unlock();
    }

    void clear()
    {
        mutex_m.lock();
        {
            queue_m.clear();
        }
        mutex_m.unlock();
    }

private:
    QQueue<T>       queue_m;
    QMutex          mutex_m;
};

#endif // SHAREDBUFFER_HH
