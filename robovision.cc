#include <robovision.hh>

roboVision::roboVision(QWidget *parent)
    : QMainWindow(parent),
      ui_m(new Ui::roboVision),
      video_widget_m(new VideoWidget(parent)),
      process_m(new Process()),
      input_video_buffer_m(new SharedBuffer<cv::Mat>()),
      output_video_buffer_m(new SharedBuffer<cv::Mat>()),
      gl_buffer_m(new SharedBuffer<GLWidget::s_cube_pos>()),
      rect_input_buffer_m(new SharedBuffer<cv::Rect>()),
      rect_output_buffer_m(new SharedBuffer<cv::Rect>()),
      cv_timer_m(new QTimer(this)),
      gl_timer_m(new QTimer(this))
{
    ui_m->setupUi(this);

    process_m->set_input_buffer(input_video_buffer_m);
    video_widget_m->set_output_buffer(input_video_buffer_m);

    process_m->set_output_video_buffer(output_video_buffer_m);
    ui_m->cvglwidget->set_input_buffer(output_video_buffer_m);

    process_m->set_output_gl_buffer(gl_buffer_m);
    ui_m->glWidget->set_input_buffer(gl_buffer_m);

    process_m->set_rect_input_buffer(rect_input_buffer_m);
    process_m->set_rect_output_buffer(rect_output_buffer_m);
    ui_m->cvglwidget->set_rect_input_buffer(rect_output_buffer_m);
    ui_m->cvglwidget->set_rect_output_buffer(rect_input_buffer_m);

    process_m->start();

    QPixmap colorPixmap(32, 32);
    colorPixmap.fill(Qt::blue);
    ui_m->colorLabel->setPixmap(colorPixmap);

    connect(gl_timer_m, SIGNAL(timeout()), ui_m->glWidget, SLOT(timeOut()));
    gl_timer_m->start(1000.0/60.0); // 60fps

    connect(cv_timer_m, SIGNAL(timeout()), ui_m->cvglwidget, SLOT(timeOut()));
    cv_timer_m->start(1000/40); // 60fps

    ui_m->playerGroup->hide();
    ui_m->stopButton->hide();
    connections();
}

void roboVision::connections()
{
    // FPS
    connect(ui_m->glWidget, SIGNAL(fpsUpdate(int)),
            ui_m->glFPS, SLOT(setNum(int)));
    connect(ui_m->cvglwidget, SIGNAL(fpsUpdate(int)),
            ui_m->cvglFPS, SLOT(setNum(int)));
    connect(video_widget_m, SIGNAL(fps_updated(int)),
            ui_m->videoFPS, SLOT(setNum(int)));
    connect(process_m, SIGNAL(fps_update(int)),
            ui_m->processFPS, SLOT(setNum(int)));

    connect(ui_m->cvglwidget, SIGNAL(clickColor(int, int)),
            process_m, SLOT(get_color(int,int)));

    // Process
    connect(ui_m->toggleButton, SIGNAL(clicked()),
            process_m, SLOT(toggle_output()));
    connect(ui_m->toleranceSpin, SIGNAL(valueChanged(int)),
            process_m, SLOT(set_tolerance(int)));
    connect(ui_m->matchBox, SIGNAL(clicked(bool)),
            process_m, SLOT(set_show_match(bool)));
    connect(ui_m->ROICheck, SIGNAL(clicked(bool)),
            process_m, SLOT(set_ROI(bool)));
    connect(ui_m->ROICheck, SIGNAL(clicked(bool)),
            ui_m->cvglwidget, SLOT(set_ROI(bool)));

    // VideoThread
    connect(video_widget_m, SIGNAL(video_fps_updated(double)),
            process_m, SLOT(set_fps(double)));
    connect(video_widget_m, SIGNAL(video_fps_updated(double)),
            this, SLOT(set_cv_timer_interval(double)));

    // open close
    connect(ui_m->openCameraButton, SIGNAL(clicked()),
            video_widget_m, SLOT(open_device()));
    connect(ui_m->openFileButton, SIGNAL(clicked()),
            video_widget_m, SLOT(open_file()));
    connect(ui_m->closeButton, SIGNAL(clicked()),
            video_widget_m, SLOT(close()));

    // start/stop
    connect(ui_m->stopButton, SIGNAL(clicked()),
            video_widget_m, SLOT(stop()));
    connect(ui_m->playButton, SIGNAL(clicked()),
            video_widget_m, SLOT(start()));
    connect(ui_m->posSlider, SIGNAL(valueChanged(int)),
            video_widget_m, SLOT(set_pos(int)));

    // UI
    connect(process_m, SIGNAL(color_changed(QColor)),
            this, SLOT(set_object_color(QColor)));
    connect(video_widget_m, SIGNAL(total_frames_updated(double)),
            this, SLOT(set_total_frames(double)));
    connect(video_widget_m, SIGNAL(opened()),
            this, SLOT(video_open()));
    connect(video_widget_m, SIGNAL(started()),
            this, SLOT(video_play()));
    connect(video_widget_m, SIGNAL(starting(bool)),
            ui_m->playButton, SLOT(setEnabled(bool)));
    connect(video_widget_m, SIGNAL(stopped()),
            this, SLOT(video_stop()));
    connect(video_widget_m, SIGNAL(stopping(bool)),
            ui_m->stopButton, SLOT(setEnabled(bool)));
    connect(video_widget_m, SIGNAL(closed()),
            this, SLOT(video_close()));
    connect(video_widget_m, SIGNAL(pos_updated(int)),
            this, SLOT(video_set_pos(int)));
    connect(ui_m->kernelSpinH, SIGNAL(valueChanged(int)),
            this, SLOT(set_smooth_height(int)));
    connect(ui_m->kernelSpinW, SIGNAL(valueChanged(int)),
            this, SLOT(set_smooth_width(int)));
    connect(ui_m->filterTypeBox, SIGNAL(currentIndexChanged(int)),
            process_m, SLOT(set_filter_type(int)));
    connect(ui_m->filterTypeBox, SIGNAL(currentIndexChanged(int)),
            this, SLOT(change_filter_type(int)));
    connect(ui_m->showFilterBox, SIGNAL(clicked(bool)),
            process_m, SLOT(set_show_filter(bool)));
}

roboVision::~roboVision()
{
  process_m->stop();
  cv_timer_m->stop();
  gl_timer_m->stop();

  delete gl_timer_m;
  delete cv_timer_m;
  delete ui_m;
  delete video_widget_m;
  delete input_video_buffer_m;
  delete output_video_buffer_m;
  delete rect_input_buffer_m;
  delete rect_output_buffer_m;
  delete gl_buffer_m;
}
