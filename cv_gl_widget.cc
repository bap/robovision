#include <cv_gl_widget.hh>
#include <QMouseEvent>
#include <QRgb>
#include <QDebug>

CVGLWidget::CVGLWidget(QWidget *parent) :
    QGLWidget(parent),
    scene_changed_m(false),
    start_flag_m(false),
    image_size_m(0, 0),
    image_pos_m(0, 0),
    ratio_m(4.0f/3.0f),
    video_buffer_m(NULL),
    rect_input_buffer_m(NULL),
    rect_output_buffer_m(NULL),
    mouse_pos_m(-1,-1),
    mouse_type_m(gl::mouse::none),
    click_type_m(Qt::NoButton),
    mouse_pressed_m(false),
    click_pos_m(0, 0),
    click_size_m(0, 0),
    selected_m(false),
    just_click_m(false),
    use_roi_m(false)
{
    timer_m.start();
    time_m = timer_m.elapsed();
}

void CVGLWidget::mouseReleaseEvent(QMouseEvent *event)
{
    mutex_m.lock();
    {
        if (event->button() == Qt::LeftButton
                || event->button() == Qt::RightButton)
        {
            click_type_m = event->button();
            mouse_type_m = gl::mouse::release;
            mouse_pressed_m = false;

            mouse_pos_m = event->pos();
        }
    }
    mutex_m.unlock();;
}

void CVGLWidget::mousePressEvent(QMouseEvent *event)
{
    mutex_m.lock();
    {
        if (event->button() == Qt::LeftButton
                || event->button() == Qt::RightButton)
        {
            click_type_m = event->button();
            mouse_type_m = gl::mouse::click;
            mouse_pressed_m = true;

            mouse_pos_m = event->pos();
        }
    }
    mutex_m.unlock();
}

void CVGLWidget::mouseMoveEvent(QMouseEvent *event)
{
    mutex_m.lock();
    {
        if (mouse_pressed_m)
        {
            mouse_pos_m = event->pos();
            mouse_type_m = gl::mouse::move;
        }
    }
    mutex_m.unlock();
}

void CVGLWidget::initializeGL()
{
    makeCurrent();
    qglClearColor(QColor::fromRgb(0, 0, 0));
}

void CVGLWidget::resizeGL(int width, int height)
{
    makeCurrent();
    glViewport(0, 0, (GLint)width, (GLint)height);

    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();

    glOrtho(0, width, 0, height, 0, 1);

    glMatrixMode(GL_MODELVIEW);

    image_size_m.setHeight(width/ratio_m);
    image_size_m.setWidth(width);

    mutex_m.lock();
    {
        if (image_size_m.height() > height)
        {
            image_size_m.setWidth(height*ratio_m);
            image_size_m.setHeight(height);
        }

        image_pos_m.setX((width-image_size_m.width()) / 2);
        image_pos_m.setY((height-image_size_m.height()) / 2);

        scene_changed_m = true;
    }
    mutex_m.unlock();

    updateScene();
}

void CVGLWidget::updateScene()
{
    mutex_m.lock();
    {
        if (scene_changed_m && isVisible())
            updateGL();
    }
    mutex_m.unlock();
}

void CVGLWidget::paintGL()
{
    makeCurrent();

    if (!scene_changed_m )
        return;

    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    renderImage();

    scene_changed_m = false;
}

void CVGLWidget::gestures()
{
    QRect ri = image_rect_m;

    // CLICK
    if (mouse_type_m == gl::mouse::click)
    {
        just_click_m = true;
        selected_m = false;

        if (click_type_m == Qt::LeftButton)
        {
            click_pos_m.setX(ri.x());
            click_pos_m.setY(ri.y());
        }

        QRect intersect(ri.x(), ri.y(), 1, 1);
        if (select_rect_m.intersects(intersect))
        {
            if (click_type_m == Qt::RightButton)
                move();

            selected_m = true;
        }
    }

    // MOVE
    if (mouse_type_m == gl::mouse::move)
    {
        just_click_m = false;
        if (selected_m)
        {
            if (click_type_m == Qt::LeftButton)
            {
                select_rect_m.moveTo(ri.x() - select_rect_m.width()/2,
                                     ri.y() - select_rect_m.height()/2);
            }
            if (click_type_m == Qt::RightButton)
                move();
        }
        else
        {
            if (click_type_m == Qt::LeftButton)
            {
                int dpx = ri.x() - click_pos_m.x();
                int dpy = ri.y() - click_pos_m.y();

                if (abs(dpx) >= 3)
                {
                    select_rect_m.setX(click_pos_m.x());
                    select_rect_m.setWidth(dpx);
                }

                if (abs(dpy) >= 3)
                {
                    select_rect_m.setY(click_pos_m.y());
                    select_rect_m.setHeight(dpy);
                }
            }
        }
    }

    check_rect();
    // RELEASE
    if (mouse_type_m == gl::mouse::release)
    {
        just_click_m = false;
        release();
    }

    if (use_roi_m)
      paint();

    mouse_type_m = gl::mouse::none;
}

void CVGLWidget::renderImage()
{
    makeCurrent();

    glClear(GL_COLOR_BUFFER_BIT);

    if (!image_m.isNull())
    {
        glLoadIdentity();

        QImage image;

        glPushMatrix();
        {
            int iw = image_m.width();
            int ih = image_m.height();

            if (iw != size().width() && ih != size().height())
            {
                image = image_m.scaled(image_size_m, Qt::IgnoreAspectRatio,
                                       Qt::SmoothTransformation);
            }
            image_m = image;

            glRasterPos2i(image_pos_m.x(), image_pos_m.y());

            iw = image_m.width();
            ih = image_m.height();

            update_image_size_and_click();
            gestures();

            glDrawPixels(iw, ih, GL_RGBA, GL_UNSIGNED_BYTE, image_m.bits());
        }
        glPopMatrix();

        glFlush();
    }
}

void CVGLWidget::timeOut()
{
  int current_time;
  int delta;

  mutex_m.lock();
  {
    current_time = timer_m.elapsed();
    delta = current_time - time_m;

    if (delta > 0)
      emit fpsUpdate(1000/delta);
  }
  mutex_m.unlock();

    mutex_m.lock();
    {
        if (video_buffer_m && video_buffer_m->dataAvailable())
            frame_m = video_buffer_m->get();

        if (rect_input_buffer_m && rect_input_buffer_m->dataAvailable())
          set_rect(rect_input_buffer_m->get());
    }
    mutex_m.unlock();

    if (frame_m.cols == 0 || frame_m.rows == 0)
    {
      mutex_m.lock();
      {
          time_m = current_time;
      }
      mutex_m.unlock();
      return;
    }

    mutex_m.lock();
    {
        ratio_m = (float)frame_m.cols/(float)frame_m.rows;

        if (frame_m.channels() == 3)
            image_m = QImage((const unsigned char*)(frame_m.data),
                             frame_m.cols, frame_m.rows,
                             frame_m.step, QImage::Format_RGB888).rgbSwapped();
        else if (frame_m.channels() == 1)
            image_m = QImage((const unsigned char*)(frame_m.data),
                             frame_m.cols, frame_m.rows,
                             frame_m.step, QImage::Format_Indexed8);
        else
            return;

        image_m = QGLWidget::convertToGLFormat(image_m);

        scene_changed_m = true;
    }
    mutex_m.unlock();

    updateScene();

    mutex_m.lock();
    {
        time_m = current_time;
    }
    mutex_m.unlock();
}

