#ifndef VideoCaptureWidget_HXX
#define VideoCaptureWidget_HXX

# include <video_thread.hh>

inline void VideoThread::set_output_buffer(SharedBuffer<cv::Mat>* buffer)
{
    buffer_m = buffer;
}

inline void VideoThread::clear_buffer()
{
  mutex_m.lock();
  {
    if (buffer_m)
      buffer_m->clear();
  }
  mutex_m.unlock();
}

inline double VideoThread::total_frames()
{
  double total;
  mutex_m.lock();
  {
    total = total_frame_m;
  }
  mutex_m.unlock();

  return total;
}

inline double VideoThread::video_fps()
{
  double fps;
  mutex_m.lock();
  {
    fps = video_fps_m;
  }
  mutex_m.unlock();

  return fps;
}

inline void VideoThread::set_pos(int pos)
{
    if (videoType_m != FILE)
        return;

    mutex_m.lock();
    {
        in_m.set(CV_CAP_PROP_POS_FRAMES, pos);
        in_m.retrieve(frame_m);
        current_frame_m = pos;
        if (buffer_m)
          buffer_m->add(frame_m.clone());
        emit pos_updated(pos);
    }
    mutex_m.unlock();
}

#endif // VideoThread_HXX
