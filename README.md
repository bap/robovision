roboVision
==========

A school project that uses OpenCV to track movement and
use that movement to control a 3D environnement.

Build
=====

Build with `qmake . && make`

Usage
=====

Select input type (camera or video file) then click on the pixel color
you want to track and the magic will do the job!

You can switch between binarize and colored output.
