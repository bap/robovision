#ifndef CVWIDGET_HXX
#define CVWIDGET_HXX

# include <cvwidget.hh>

inline void CVWidget::setBuffer(SharedBuffer<cv::Mat>* buffer)
{
    buffer_m = buffer;
}

inline void CVWidget::setRectBuffer(SharedBuffer<cv::Rect> *buffer)
{
    rect_buffer_m = buffer;
}


inline void CVWidget::setGLBuffer(SharedBuffer<GLWidget::s_cube_pos> *buffer)
{
    gl_buffer_m = buffer;
}

inline void CVWidget::checkRect()
{
    if (rect_m.x + rect_m.width >= frame_m.cols)
        rect_m.x = frame_m.cols - rect_m.width;
    if (rect_m.y + rect_m.height >= frame_m.rows)
        rect_m.y =  frame_m.rows - rect_m.height;

    if (rect_m.x < 0)
        rect_m.x = 0;
    if (rect_m.y < 0)
        rect_m.y = 0;
}

inline void CVWidget::setRectPos(int x, int y)
{
    rect_m.x = x;
    rect_m.y = y;
    rect_m.width = size_m.width;
    rect_m.height = size_m.height;

    checkRect();

    emit rectSize(rect_m.x, rect_m.y, rect_m.width, rect_m.height);
}

inline void CVWidget::setRect(int x, int y, int w, int h)
{
    mutex_m.lock();
    {
        size_m.width = w;
        size_m.height = h;

        rect_m.width = w;
        rect_m.height = h;
        rect_m.x = x;
        rect_m.y = y;

        checkRect();

//        if (x + w > frame_m.cols)
//        {
//            rect_m.width = frame_m.cols - x;
//        }
//        if (y + h > frame_m.rows)
//        {
//            rect_m.height = frame_m.rows - y;
//        }
    }
    mutex_m.unlock();

    //emit rectSize(rect_m.x, rect_m.y, rect_m.width, rect_m.height);
}

inline void CVWidget::setROI(bool b)
{
    mutex_m.lock();
    {
        use_roi_m = b;
    }
    mutex_m.unlock();
}

inline int CVWidget::distance(const cv::Point &p1, const cv::Point &p2)
{
    int dx = p2.x - p1.x;
    int dy = p2.y - p1.y;

    return sqrt(dx*dx + dy*dy);
}

inline void CVWidget::neighbors(int x, int y)
{
    // north east
    neighbors_m[0].x = x + 1;
    neighbors_m[0].y = y - 1;

    // east
    neighbors_m[1].x = x + 1;
    neighbors_m[1].y = y;

    // south east
    neighbors_m[2].x = x + 1;
    neighbors_m[2].y = y + 1;

     // south west
    neighbors_m[3].x = x - 1;
    neighbors_m[3].y = y + 1;

     // west
    neighbors_m[4].x = x - 1;
    neighbors_m[4].y = y;

    // north west
    neighbors_m[5].x = x - 1;
    neighbors_m[5].y = y - 1;

    // north
    neighbors_m[6].x = x;
    neighbors_m[6].y = y - 1;

    // south
    neighbors_m[7].x = x;
    neighbors_m[7].y = y + 1;
}

inline QColor CVWidget::random_color()
{
    return QColor(qrand() % 255, qrand() % 255, qrand() % 255);
}

inline void CVWidget::setPos(int pos)
{
    if (videoType_m != FILE)
        return;

    stop();
    mutex_m.lock();
    {
        in_m.set(CV_CAP_PROP_POS_FRAMES, pos);
        in_m >> frame_m;
        buffer_m->add(frame_m.clone());
        int pos = in_m.get(CV_CAP_PROP_POS_FRAMES);
        emit newPos(pos);
    }
    mutex_m.unlock();
}

inline void CVWidget::changeTolerance(int t)
{
    mutex_m.lock();
    {
        tolerance_m = t;
    }
    mutex_m.unlock();
}

inline void CVWidget::changeThreshold(int v)
{
    mutex_m.lock();
    {
        threshold_m = v;
    }
    mutex_m.unlock();
}

inline void CVWidget::changeMatch(bool b)
{
    mutex_m.lock();
    {
        show_match_m = b;
    }
    mutex_m.unlock();
}

inline void CVWidget::toggleOutput()
{
    if (outputType_m == VIDEO)
        outputType_m = MASK;
    else
        outputType_m = VIDEO;
}

#endif // CVWIDGET_HXX
