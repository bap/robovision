#ifndef PROCESS_HH
#define PROCESS_HH

# include <QObject>
# include <QThread>
# include <QMutex>
# include <QColor>
# include <QVector>

# include <sharedbuffer.hh>
# include <glwidget.hh>

# include <opencv/cv.h>
# include <opencv/highgui.h>

namespace process
{
  typedef struct hsv
  {
    int h;
    int s;
    int v;
  } hsv;

  namespace filter
  {
    enum type
    {
      none = 0,
      blur_no_scale,
      blur,
      gaussian,
      median,
      bilateral
    };
  }
}


class Process : public QThread
{
  Q_OBJECT
public:
  explicit Process(QObject *parent = 0);

  void set_input_buffer(SharedBuffer<cv::Mat>* buffer);
  void set_output_gl_buffer(SharedBuffer<GLWidget::s_cube_pos>* buffer);
  void set_output_video_buffer(SharedBuffer<cv::Mat>* buffer);

  void set_rect_input_buffer(SharedBuffer<cv::Rect>* buffer);
  void set_rect_output_buffer(SharedBuffer<cv::Rect>* buffer);

  void stop();

protected:
  void run();
  void set_rect_pos(int x, int y);
  void check_rect();
  void labelize(cv::Mat& mask);
  cv::Point detect_color(cv::Mat& mask);
  cv::Point process();
  QColor random_color();
  void neighbors(int x, int y);
  void apply_filter();

signals:
  void color_changed(QColor color);
  void fps_update(int fps);

public slots:
  void toggle_output();
  void set_tolerance(int t);
  void set_show_match(bool b);
  void get_color(int x, int y);

  void set_rect(const cv::Rect &rect);
  void set_ROI(bool b);

  void set_filter_type(int type);
  void set_smotth_height(int h);
  void set_smotth_width(int w);
  void set_fps(double fps);

  void set_show_filter(bool b);

private:
  enum OutputType
  {
      VIDEO,
      MASK
  }                   output_type_m;
  QVector<cv::Point>  neighbors_m;
  bool                start_flag_m;
  cv::Mat             frame_m;
  cv::Mat             copy_frame_m;
  cv::Mat             filter_frame_m;
  QMutex              mutex_m;
  process::hsv        hsv_m;
  int                 tolerance_m;
  QColor              color_m;
  bool                show_match_m;
  QList<cv::Point>    cloud_m;
  bool                use_roi_m;
  cv::Rect            rect_m;
  cv::Size            size_m;
  cv::Point           pos_m;
  int                 filter_type_m;
  cv::Size            smooth_params_m;
  int                 fps_m;
  QTime               timer_m;
  int                 time_m;
  bool                show_filter_m;
  cv::Rect            detect_rect_m;

  SharedBuffer<cv::Mat>*              input_buffer_m;
  SharedBuffer<cv::Mat>*              output_video_buffer_m;
  SharedBuffer<GLWidget::s_cube_pos>* output_gl_buffer_m;
  SharedBuffer<cv::Rect>*             rect_input_buffer_m;
  SharedBuffer<cv::Rect>*             rect_output_buffer_m;
};

# include <process.hxx>

# endif // PROCESS_HH
