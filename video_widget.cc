#include "video_widget.hh"

VideoWidget::VideoWidget(QObject *parent) :
  QObject(parent),
  buffer_m(NULL)
{
  connect(&thread_m, SIGNAL(pos_updated(int)),
          this, SIGNAL(pos_updated(int)));
  connect(&thread_m, SIGNAL(fps_updated(int)),
          this, SIGNAL(fps_updated(int)));
}

VideoWidget::~VideoWidget()
{
  close();
}

void VideoWidget::set_output_buffer(SharedBuffer<cv::Mat>* buffer)
{
    buffer_m = buffer;
    thread_m.set_output_buffer(buffer);
}

void VideoWidget::open()
{
  thread_m.open();

  emit pos_updated(0);
  emit video_fps_updated(thread_m.video_fps());
  emit total_frames_updated(thread_m.total_frames());
}

void VideoWidget::open_file()
{
  if (thread_m.open_file())
  {
    open();
    emit opened();
  }
}

void VideoWidget::open_device()
{
  if (thread_m.open_device())
  {
    open();
    emit opened();
  }
}

void VideoWidget::close()
{
  stop();
  thread_m.close();
  emit closed();
}

void VideoWidget::stop()
{
  emit stopping(false);
  thread_m.stop();
  thread_m.wait();
  thread_m.clear_buffer();
  emit stopped();
}

void VideoWidget::start()
{
  emit starting(false);
  if (thread_m.play())
    emit started();
}

void VideoWidget::set_pos(int pos)
{
  stop();
  thread_m.set_pos(pos);
}
