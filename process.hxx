#ifndef PROCESS_HXX
#define PROCESS_HXX

# include <process.hh>

inline void Process::set_input_buffer(SharedBuffer<cv::Mat>* buffer)
{
  input_buffer_m = buffer;
}

inline void Process::set_output_gl_buffer(SharedBuffer<GLWidget::s_cube_pos>* buffer)
{
  output_gl_buffer_m = buffer;
}

inline void Process::set_output_video_buffer(SharedBuffer<cv::Mat> *buffer)
{
  output_video_buffer_m = buffer;
}

inline void Process::set_rect_input_buffer(SharedBuffer<cv::Rect>* buffer)
{
  rect_input_buffer_m = buffer;
}

inline void Process::set_rect_output_buffer(SharedBuffer<cv::Rect>* buffer)
{
  rect_output_buffer_m = buffer;
}

inline void Process::set_smotth_width(int w)
{
  mutex_m.lock();
  {
    smooth_params_m.width = w;
  }
  mutex_m.unlock();
}

inline void Process::set_show_filter(bool b)
{
  mutex_m.lock();
  {
    show_filter_m = b;
  }
  mutex_m.unlock();
}

inline void Process::set_fps(double fps)
{
  mutex_m.lock();
  {
    fps_m = fps;
  }
  mutex_m.unlock();
}

inline void Process::set_smotth_height(int h)
{
  mutex_m.lock();
  {
    smooth_params_m.height = h;
  }
  mutex_m.unlock();
}

inline void Process::set_filter_type(int type)
{
  mutex_m.lock();
  {
    filter_type_m = type;
  }
  mutex_m.unlock();
}


inline void Process::set_rect_pos(int x, int y)
{
    rect_m.x = x;
    rect_m.y = y;
    rect_m.width = size_m.width;
    rect_m.height = size_m.height;

    check_rect();

    if (rect_output_buffer_m)
      rect_output_buffer_m->add(rect_m);
}

inline void Process::set_rect(const cv::Rect& rect)
{
  rect_m = rect;

  check_rect();

  size_m.width = rect.width;
  size_m.height = rect.height;
}

inline void Process::check_rect()
{
    if (rect_m.x + rect_m.width >= frame_m.cols)
        rect_m.x = frame_m.cols - rect_m.width;
    if (rect_m.y + rect_m.height >= frame_m.rows)
        rect_m.y =  frame_m.rows - rect_m.height;

    if (rect_m.x < 0)
        rect_m.x = 0;
    if (rect_m.y < 0)
        rect_m.y = 0;
    if (rect_m.width <= 0)
      rect_m.width = 1;
    if (rect_m.height <= 0)
      rect_m.height = 1;
}

inline void Process::set_ROI(bool b)
{
    mutex_m.lock();
    {
        use_roi_m = b;
    }
    mutex_m.unlock();
}

inline void Process::neighbors(int x, int y)
{
    // north east
    neighbors_m[0].x = x + 1;
    neighbors_m[0].y = y - 1;

    // east
    neighbors_m[1].x = x + 1;
    neighbors_m[1].y = y;

    // south east
    neighbors_m[2].x = x + 1;
    neighbors_m[2].y = y + 1;

     // south west
    neighbors_m[3].x = x - 1;
    neighbors_m[3].y = y + 1;

     // west
    neighbors_m[4].x = x - 1;
    neighbors_m[4].y = y;

    // north west
    neighbors_m[5].x = x - 1;
    neighbors_m[5].y = y - 1;

    // north
    neighbors_m[6].x = x;
    neighbors_m[6].y = y - 1;

    // south
    neighbors_m[7].x = x;
    neighbors_m[7].y = y + 1;
}

inline QColor Process::random_color()
{
    return QColor(qrand() % 255, qrand() % 255, qrand() % 255);
}

inline void Process::set_tolerance(int t)
{
    mutex_m.lock();
    {
        tolerance_m = t;
    }
    mutex_m.unlock();
}

inline void Process::set_show_match(bool b)
{
    mutex_m.lock();
    {
        show_match_m = b;
    }
    mutex_m.unlock();
}

inline void Process::toggle_output()
{
    if (output_type_m == VIDEO)
        output_type_m = MASK;
    else
        output_type_m = VIDEO;
}

inline void Process::get_color(int x, int y)
{
  cv::Vec3b   color;

  mutex_m.lock();
  {
    if (frame_m.cols > 0 && frame_m.rows > 0)
    {
      pos_m = cv::Point(x, y);
      color = copy_frame_m.at<cv::Vec3b>(y, x);
      color_m = QColor(color[2], color[1], color[0]);
      emit color_changed(color_m);

      cv::Mat hsv = frame_m.clone();

      cv::cvtColor(frame_m, hsv, CV_BGR2HSV);
      cv::Vec3b pixel = hsv.at<cv::Vec3b>(y, x);

      hsv_m.h = pixel[0];
      hsv_m.s = pixel[1];
      hsv_m.v = pixel[2];

      hsv.release();
    }
  }
  mutex_m.unlock();
}

#endif // PROCESS_HXX
