#ifndef ROBOVISION_HXX
#define ROBOVISION_HXX

# include <robovision.hh>

inline void roboVision::set_total_frames(double n)
{
    ui_m->posSlider->blockSignals(true);
    ui_m->totalFrame->setNum(n);
    if (n > 0)
        ui_m->posSlider->setMaximum(n-1);
    else
        ui_m->posSlider->setMaximum(0);
    ui_m->posSlider->setValue(0);
    ui_m->currentFrame->setNum(0);
    ui_m->posSlider->blockSignals(false);
}

inline void roboVision::set_cv_timer_interval(double interval)
{
  cv_timer_m->stop();
  cv_timer_m->start(1000/interval);
}

inline void roboVision::video_open()
{
    ui_m->playerGroup->show();
    ui_m->sourceGroup->hide();
}

inline void roboVision::video_close()
{
    ui_m->playerGroup->hide();
    ui_m->sourceGroup->show();
}

inline void roboVision::video_stop()
{
  gl_buffer_m->clear();
  ui_m->playButton->setEnabled(true);
  ui_m->stopButton->hide();
  ui_m->playButton->show();
}

inline void roboVision::video_set_pos(int pos)
{
    ui_m->posSlider->blockSignals(true);
    ui_m->posSlider->setValue(pos);
    ui_m->currentFrame->setNum(pos);
    ui_m->posSlider->blockSignals(false);
}

inline void roboVision::video_play()
{
  ui_m->stopButton->setEnabled(true);
  ui_m->stopButton->show();
  ui_m->playButton->hide();
}

inline void roboVision::set_object_color(QColor color)
{
    QPixmap colorPixmap(32, 32);
    colorPixmap.fill(color);
    ui_m->colorLabel->setPixmap(colorPixmap);
}

inline void roboVision::set_ROI_infos(int x, int y, int w, int h)
{
    ui_m->ROIPos->setText("x=" + QString::number(x)
                          + ", y=" + QString::number(y));
    ui_m->ROISize->setText(QString::number(w)
                           + "x" + QString::number(h));

    //ui_m->cvglwidget->set_rect(x, y, w, h);
}

inline void roboVision::set_smooth_height(int h)
{
  if (h % 2 == 0)
  {
    ui_m->kernelSpinH->blockSignals(true);
    ++h;
    ui_m->kernelSpinH->setValue(h);
    ui_m->kernelSpinH->blockSignals(false);
  }
  process_m->set_smotth_height(h);
}

inline void roboVision::set_smooth_width(int w)
{
  if (w % 2 == 0)
  {
    ui_m->kernelSpinW->blockSignals(true);
    ++w;
    ui_m->kernelSpinW->setValue(w);
    ui_m->kernelSpinW->blockSignals(false);
  }
  process_m->set_smotth_width(w);
}

inline void roboVision::change_filter_type(int index)
{
  ui_m->kernelSpinW->setEnabled(true);
  ui_m->kernelSpinH->setEnabled(true);

  if (index == process::filter::bilateral || index == process::filter::median)
    ui_m->kernelSpinH->setEnabled(false);

  if (index == process::filter::none)
  {
    ui_m->kernelSpinH->setEnabled(false);
    ui_m->kernelSpinW->setEnabled(false);
   }
}

#endif // ROBOVISION_HXX
