#ifndef CV_GL_WIDGET_HH_
#define CV_GL_WIDGET_HH_

# include <QGLWidget>
# include <QTime>
# include <QThread>

# include <opencv/cv.h>

# include <sharedbuffer.hh>

namespace gl
{
    namespace mouse
    {
        enum mouseType
        {
            none,
            click,
            move,
            release
        };
    }
}

class CVGLWidget : public QGLWidget
{
    Q_OBJECT
public:
    explicit CVGLWidget(QWidget *parent = 0);

//    void setBuffer(SharedBuffer<cv::Mat>* buffer);
//    void setRectBuffer(SharedBuffer<cv::Rect>* buffer);
//    void setRect(int x, int y, int w, int h);

    void set_input_buffer(SharedBuffer<cv::Mat>* buffer);
    void set_rect_input_buffer(SharedBuffer<cv::Rect>* buffer);
    void set_rect_output_buffer(SharedBuffer<cv::Rect>* buffer);

signals:
    void    fpsUpdate(int fps);
    void    clickColor(int x, int y);
    void    selectRect(int x, int y, int w, int h);

public slots:
    void    timeOut();
    void    set_ROI(bool b);

protected:
    void 	initializeGL();
    void 	paintGL();
    void 	resizeGL(int width, int height);
    void    mouseReleaseEvent(QMouseEvent* event);
    void    mousePressEvent(QMouseEvent* event);
    void    mouseMoveEvent(QMouseEvent* event);
    void    updateScene();
    void    renderImage();
    void    gestures();
    void    release();
    void    paint();
    void    move();
    void    update_image_size_and_click();
    void    check_rect();
    void    set_rect(const cv::Rect& rect);

private:
    bool                    scene_changed_m;
    bool                    start_flag_m;

    QImage                  image_m;
    cv::Mat                 frame_m;

    QSize                   image_size_m;
    QPoint                  image_pos_m;

    float                   ratio_m;

    SharedBuffer<cv::Mat>*  video_buffer_m;
    SharedBuffer<cv::Rect>* rect_input_buffer_m;
    SharedBuffer<cv::Rect>* rect_output_buffer_m;
    QTime                   timer_m;
    int                     time_m;

    QPoint                  mouse_pos_m;
    gl::mouse::mouseType    mouse_type_m;
    Qt::MouseButton         click_type_m;
    bool                    mouse_pressed_m;
    QRect                   select_rect_m;
    QPoint                  click_pos_m;
    QSize                   click_size_m;
    bool                    selected_m;
    bool                    just_click_m;
    QRect                   image_rect_m;
    QMutex                  mutex_m;
    bool                    use_roi_m;
};

# include <cv_gl_widget.hxx>

#endif // CV_GL_WIDGET_HH_
