#-------------------------------------------------
#
# Project created by QtCreator 2012-10-29T17:34:17
#
#-------------------------------------------------

QT       += core gui opengl

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = roboVision
TEMPLATE = app

LIBS += -lGLU `pkg-config --libs opencv`


SOURCES += main.cc          \
           robovision.cc    \
           glwidget.cc \
    cv_gl_widget.cc \
    process.cc \
    video_thread.cc \
    video_widget.cc

HEADERS  += glwidget.hh     \
            robovision.hh \
    cv_gl_widget.hh \
    sharedbuffer.hh \
    cv_gl_widget.hxx \
    process.hh \
    process.hxx \
    robovision.hxx \
    video_thread.hh \
    video_thread.hxx \
    video_widget.hh

FORMS    += robovision.ui

RESOURCES += \
    resources.qrc
