#ifndef CVWIDGET_HH
#define CVWIDGET_HH

# include <QThread>
# include <QMutex>
# include <QQueue>
# include <QTime>
# include <QColor>
# include <QVector>
# include <QRect>

# include <opencv/cv.h>
# include <opencv/highgui.h>

# include <sharedbuffer.hh>
# include <glwidget.hh>

class VideoThread : public QThread
{
    Q_OBJECT

    // PUBLIC
public:
    explicit VideoThread(QObject *parent = 0);
    ~VideoThread();

    void set_output_buffer(SharedBuffer<cv::Mat>* buffer);
    void clear_buffer();

    bool open_file();
    bool open_device();
    void open();

    bool play();
    void stop();
    void close();

    void set_pos(int pos);

    double video_fps();
    double total_frames();

    // PRIVATE
private:
    enum VideoType
    {
        DEVICE,
        FILE
    }                   videoType_m;

    QString             file_m;
    QMutex              mutex_m;
    cv::VideoCapture    in_m;
    bool                startFlag_m;
    QTime               timer_m;
    int                 time_m;

    cv::Mat             frame_m;

    SharedBuffer<cv::Mat>*  buffer_m;
    double              video_fps_m;
    double              current_frame_m;
    double              total_frame_m;

    void run();

signals:
    void fps_updated(int fps);
    void pos_updated(int pos);
};

# include <video_thread.hxx>

#endif // CVWIDGET_HH
