#include <process.hh>

Process::Process(QObject *parent)
  : QThread(parent),
    output_type_m(VIDEO),
    neighbors_m(8),
    start_flag_m(false),
    tolerance_m(10),
    show_match_m(false),
    use_roi_m(false),
    rect_m(0, 0, 1, 1),
    size_m(1, 1),
    filter_type_m(process::filter::none),
    smooth_params_m(3, 3),
    fps_m(40.0f),
    show_filter_m(false),
    input_buffer_m(NULL),
    output_video_buffer_m(NULL),
    output_gl_buffer_m(NULL),
    rect_input_buffer_m(NULL),
    rect_output_buffer_m(NULL)
{
  pos_m.x = 0;
  pos_m.y = 0;

  timer_m.start();
  time_m = timer_m.elapsed();
}

void Process::stop()
{
  mutex_m.lock();
  {
    start_flag_m = false;
    //wait();
  }
  mutex_m.unlock();
}

void Process::apply_filter()
{
  cv::Mat src_mat = filter_frame_m.clone();
  IplImage src_img = src_mat;
  IplImage dst_img;
  if (show_filter_m)
    dst_img = frame_m;
  else
    dst_img = filter_frame_m;

  int cv_filter = CV_BILATERAL;

  switch (filter_type_m)
  {
    case process::filter::bilateral:
      cv_filter = CV_BILATERAL;
      break;
    case process::filter::blur:
      cv_filter = CV_BLUR;
      break;
    case process::filter::blur_no_scale:
      cv_filter = CV_BLUR_NO_SCALE;
      break;
    case process::filter::gaussian:
      cv_filter = CV_GAUSSIAN;
      break;
    case process::filter::median:
      cv_filter = CV_MEDIAN;
      break;
  }

  cvSmooth(&src_img, &dst_img, cv_filter,
           smooth_params_m.width,
           smooth_params_m.height);
}

void Process::run()
{
  cv::Point p;
  GLWidget::s_cube_pos cube_pos;
  int current_time;
  double delta;

  start_flag_m = true;

  while (start_flag_m)
  {
    current_time = timer_m.elapsed();
    delta = current_time - time_m;

    if (delta >= 1000.0f/fps_m)
    {
      emit fps_update(1000/delta);

      mutex_m.lock();
      {
        if (input_buffer_m && input_buffer_m->dataAvailable())
        {
          frame_m = input_buffer_m->get();
          copy_frame_m = frame_m.clone();
          filter_frame_m = frame_m.clone();

          if (filter_type_m != process::filter::none)
            apply_filter();


          if (use_roi_m && rect_input_buffer_m
              && rect_input_buffer_m->dataAvailable())
          {
            set_rect(rect_input_buffer_m->get());
          }

          p = process();

          if (p.x >= 0 && p.y >= 0)
          {
            if (use_roi_m)
              set_rect_pos(detect_rect_m.x, detect_rect_m.y);
//                    p.x-rect_m.width/2, p.y-rect_m.width/2);

            cv::rectangle(frame_m, detect_rect_m, CV_RGB(255, 0, 0), 2);

            if (use_roi_m && output_type_m != MASK)
              cv::rectangle(frame_m, rect_m, CV_RGB(0, 0, 255));
//            else if (output_type_m != MASK)
//            {
//              cv::rectangle(frame_m, cv::Rect(p.x, p.y, 1, 1),
//                            CV_RGB(255, 0, 0));
//            }

            cube_pos.x = (float)p.x / frame_m.cols;
            cube_pos.y = (float)p.y / frame_m.rows;
            if (output_gl_buffer_m)
              output_gl_buffer_m->add(cube_pos);
          }
        }

        if (output_video_buffer_m)
          output_video_buffer_m->add(frame_m);

      }
      mutex_m.unlock();

      time_m = current_time;
    }
    else
      msleep(1000/fps_m - delta);
  }
}

void Process::labelize(cv::Mat& mask)
{
  QStack<cv::Point> stack;
  QList<cv::Point> biggest_cloud;
  QColor rc;
  cv::Vec3b vcolor;
  cv::Point p;

  cloud_m.clear();

  for (int x = 0; x < mask.cols; x++)
  {
    for(int y = 0; y < mask.rows; y++)
    {
      if (mask.at<unsigned char>(y, x) == 255) // not marked
      {
        biggest_cloud.clear();
        stack.append(cv::Point(x, y));
        rc = random_color();
        vcolor = cv::Vec3b(rc.blue(), rc.green(), rc.red());

        while (!stack.isEmpty())
        {
          p = stack.pop();

          // already marked
          if (mask.at<unsigned char>(p.y, p.x) == 0)
            continue;

          mask.at<unsigned char>(p.y, p.x) = 0;
          if (output_type_m == MASK)
          {
            if (use_roi_m)
              frame_m(rect_m).at<cv::Vec3b>(p.y, p.x) = vcolor;
            else
              frame_m.at<cv::Vec3b>(p.y, p.x) = vcolor;
          }

          biggest_cloud.append(cv::Point(p.x, p.y));

          // iterates on all neighbors of the pixel
          neighbors(p.x, p.y);
          foreach (cv::Point pn, neighbors_m)
          {
            if (pn.x >= 0 && pn.x < mask.cols
                && pn.y >= 0 && pn.y < mask.rows)
            {
              if (mask.at<unsigned char>(pn.y, pn.x) == 255)
              {
                stack.append(cv::Point(pn.x, pn.y));
              }
            }
          }
        }

        // found a bigger cloud?
        if (biggest_cloud.size() > cloud_m.size())
          cloud_m = biggest_cloud;
      }
    }
  }
}

cv::Point Process::detect_color(cv::Mat& mask)
{
  // labelize all regions
  cv::Mat label_mask = mask.clone();
  labelize(label_mask);
  label_mask.release();

  int sum_x = 0;
  int sum_y = 0;
  int n_pixels = 0;
  cv::Vec3b vcolor;

  if (output_type_m == MASK)
    vcolor = cv::Vec3b(255, 255, 255);
  else
    vcolor = cv::Vec3b(0, 255, 0);
  // for each point of the cloud, sum coordinates

  int x_min = frame_m.cols;
  int x_max = 0;
  int y_min = frame_m.rows;
  int y_max = 0;

  if (use_roi_m)
  {
    x_min = rect_m.x + rect_m.width;
    x_max = rect_m.x;
    y_min = rect_m.y + rect_m.height;
    y_min = rect_m.y;
  }
  foreach (cv::Point p, cloud_m)
  {
    if (show_match_m)
    {
      if (use_roi_m)
        frame_m(rect_m).at<cv::Vec3b>(p.y, p.x) = vcolor;
      else
        frame_m.at<cv::Vec3b>(p.y, p.x) = vcolor;
    }

    if (use_roi_m)
    {
      p.x += rect_m.x;
      p.y += rect_m.y;
    }

    if (p.x < x_min)
      x_min = p.x;
    if (p.y < y_min)
      y_min = p.y;

    if (p.x > x_max)
      x_max = p.x;
    if (p.y > y_max)
      y_max = p.y;

    sum_x += p.x;
    sum_y += p.y;
    ++n_pixels;
  }


  if (n_pixels > 0)
  {
    detect_rect_m.x = x_min;
    detect_rect_m.y = y_min;
    detect_rect_m.width = x_max - x_min;
    detect_rect_m.height = y_max - y_min;
    return cv::Point(sum_x / n_pixels, sum_y / n_pixels);
  }

  return pos_m;
}

cv::Point Process::process()
{
  cv::Mat mask;
  cv::Mat hsv;

  cv::Mat frame_roi;
  if (use_roi_m)
  {
    if (show_filter_m)
      frame_roi = frame_m(rect_m).clone();
    else
      frame_roi = filter_frame_m(rect_m).clone();
  }
  else
  {
    if (show_filter_m)
      frame_roi = frame_m.clone();
    else
      frame_roi = filter_frame_m.clone();
  }

  // create the HSV matrix
  hsv = frame_roi;
  cv::cvtColor(frame_roi, hsv, CV_BGR2HSV);

  mask.create(frame_roi.rows, frame_roi.cols, CV_8UC1); // b&w mask

  // check hsv range
  cv::inRange(hsv,
              cv::Scalar(hsv_m.h - tolerance_m - 1, hsv_m.s - tolerance_m, 0),
              cv::Scalar(hsv_m.h + tolerance_m -1, hsv_m.s + tolerance_m, 255),
              mask);

  // dilate and erode the mask
  cv::Mat kernel =  cv::getStructuringElement(cv::MORPH_ELLIPSE,
                                              cv::Size(5, 5),
                                              cv::Point(2, 2));
  kernel.setTo(cv::Scalar(1));
  cv::dilate(mask, mask, kernel);
  cv::erode(mask, mask, kernel);

  // if we're visualizing the mask, empty the frame with black
  if (output_type_m == MASK)
  {
    for(int x = 0; x < frame_m.cols; x++)
    {
      for(int y = 0; y < frame_m.rows; y++)
      {
        frame_m.at<cv::Vec3b>(y, x) = cv::Vec3b(0, 0, 0);
      }
    }
  }

  cv::Point p = detect_color(mask);

  kernel.release();
  hsv.release();
  mask.release();

  return p;
}
